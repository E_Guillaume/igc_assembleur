#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <math.h>

using namespace cv;

struct Plaque_info {
	float x;
	float y;
	int date;
};

struct Param_Assemblage {
	std::string nom_output;
	int x_borne_min, x_borne_max, y_borne_min, y_borne_max;
};

Plaque_info return_info(std::string nom_mat)
{
	Plaque_info ma_plaque_info;
	std::string s_x = "",
		s_y = "",
		s_d = "";
	bool date_ex = false; //si il y a une date
	int x, y, z;
	int cpt_info = 1;
	int i;

	//on regarde si il y a la date de stocké
	i = 0;
	while ((nom_mat[i] != '.') && (i < nom_mat.size()))
	{
		if (nom_mat[i] == '-')
			cpt_info++;

		i++;
	}

	if (cpt_info >= 3)
		date_ex = true;
	//on separe les infos
	i = 0;
	cpt_info = 1;
	while ((nom_mat[i] != '.') && (i < nom_mat.size()))
	{
		if (nom_mat[i] == '-')
			cpt_info++;
		else if (cpt_info == 1)
			s_x += nom_mat[i];
		else if (cpt_info == 2)
			s_y += nom_mat[i];
		else if (cpt_info == 3)
			s_d += nom_mat[i];

		i++;
	}


	if (s_x.size() == 2)
		ma_plaque_info.x = std::stoi(s_x);
	else if (s_x.size() == 5)
	{
		int t1, t2;
		t1 = (s_x[0] - 48) * 10 + (s_x[1] - 48);
		t2 = (s_x[3] - 48) * 10 + (s_x[4] - 48);

		ma_plaque_info.x = (t1 + t2) / 2.0;
		std::cout << "cas x/2,  = " << s_x.size() << std::endl;
	}
	else
		std::cout << "error parsing x, size = " << s_x.size() << std::endl;

	if (s_y.size() == 2)
		ma_plaque_info.y = std::stoi(s_y);
	else if (s_y.size() == 5)
	{
		int t1, t2;
		t1 = (s_y[0] - 48) * 10 + (s_y[1] - 48);
		t2 = (s_y[3] - 48) * 10 + (s_y[4] - 48);

		ma_plaque_info.y = (t1 + t2) / 2.0;
		std::cout << "cas y/2,  = " << s_y.size() << std::endl;
	}
	else
		std::cout << "error parsing y, size = " << s_x.size() << std::endl;

	if (date_ex)
		ma_plaque_info.date = std::stoi(s_d);



	std::cout << "x : " << ma_plaque_info.x << ",  y : " << ma_plaque_info.y << ",  date : " << ma_plaque_info.date << std::endl;

	return ma_plaque_info;
}

Param_Assemblage return_param_console(Plaque_info *info_plaques, int nombre_plaques)
{
	Param_Assemblage mes_parametres;
	
	//definir les bornes min et max possible
	float x_max = 0, x_min = 1000, y_max = 0, y_min = 1000;
	for (int i = 1; i < nombre_plaques + 1; i++)
	{
		if (info_plaques[i - 1].x > x_max)
			x_max = info_plaques[i - 1].x;
		if (info_plaques[i - 1].x < x_min)
			x_min = info_plaques[i - 1].x;
		if (info_plaques[i - 1].y > y_max)
			y_max = info_plaques[i - 1].y;
		if (info_plaques[i - 1].y < y_min)
			y_min = info_plaques[i - 1].y;
	}
	
	
	
	
	std::cout << "xmax = " << x_max << ", xmin = " << x_min << ", ymax = " << y_max << ", ymin = " << y_min << std::endl;
	//encadrement de la ou on va generer la carte
	std::cout << "entrez le nom de la carte : ";
	std::cin >> mes_parametres.nom_output;
	mes_parametres.nom_output += ".jpg";
	
	std::cout << "entrez x_borne_min : ";
	std::cin >> mes_parametres.x_borne_min;

	std::cout << "entrez x_borne_max : ";
	std::cin >> mes_parametres.x_borne_max;

	std::cout << "entrez y_borne_min : ";
	std::cin >> mes_parametres.y_borne_min;

	std::cout << "entrez y_borne_max : ";
	std::cin >> mes_parametres.y_borne_max;
	
	
	return mes_parametres;
}

Point coord_dans_carte_ile_de_france(int x, int y)
{
	int taille_x = 4359;
	int taille_y = 4372;

	int debut_x = 59;
	int debut_y = 56;

	int fin_x = 4301;
	int fin_y = 4291;

	int nbr_x = 60;
	int nbr_y = 90;

	double taille_planche_x = ((double)fin_x - (double)debut_x) / (double)nbr_x;
	double taille_planche_y = ((double)fin_y - (double)debut_y) / (double)nbr_y;
	return Point(round((double)taille_planche_x / 2 + (double)debut_x + (x - 1)*(double)taille_planche_x), 
		round((double)taille_planche_y / 2 + (double)debut_y + (y - 1)*(double)taille_planche_y));
}

Param_Assemblage return_param_interface(Plaque_info *info_plaques, int nombre_plaques, char**argv)
{
	Param_Assemblage mes_parametres;
	//chargement image en dur pour le moment, on verra par la suite
	Mat carte_ile_de_france;
	Size size(4359, 4372);
	carte_ile_de_france= imread("C:\\Users\\guillaume\\Documents\\programation\\Assemblage Planches IGC\\x64\\Debug\\igc-tableau-assemblage.jpg", IMREAD_UNCHANGED);
	resize(carte_ile_de_france, carte_ile_de_france, size);
	namedWindow("Assemblage", WINDOW_NORMAL);
	
	for (int i = 1; i < nombre_plaques + 1; i++)
	{
		circle(carte_ile_de_france, coord_dans_carte_ile_de_france(info_plaques[i - 1].x, info_plaques[i - 1].y), 10, Scalar(255, 0, 0), 20);
	}

	imwrite("carte ile de france avec points.jpg", carte_ile_de_france);

	while (1)
	{
		imshow("Assemblage", carte_ile_de_france);
		waitKey(0);
	}
	
	

	system("PAUSE");
	//definir les bornes min et max possible
	float x_max = 0, x_min = 1000, y_max = 0, y_min = 1000;
	for (int i = 1; i < nombre_plaques + 1; i++)
	{
		if (info_plaques[i - 1].x > x_max)
			x_max = info_plaques[i - 1].x;
		if (info_plaques[i - 1].x < x_min)
			x_min = info_plaques[i - 1].x;
		if (info_plaques[i - 1].y > y_max)
			y_max = info_plaques[i - 1].y;
		if (info_plaques[i - 1].y < y_min)
			y_min = info_plaques[i - 1].y;
	}
	return mes_parametres;
}

std::string retire_chemin(std::string ma_string) //optimisation minim possible en partant du sens inverse
{
	std::string string_sans_chemin;
	for (int i = 0; i < ma_string.size(); i++)
	{
		if ((ma_string[i] == '/') || (ma_string[i] == '\\'))
		{
			string_sans_chemin = "";
			for (int j = i + 1; j < ma_string.size(); j++)
				string_sans_chemin += ma_string[j];
		}
	}
	return string_sans_chemin;

}

int main(int argc, char** argv) {

	///////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\////////////////////////////
	int nombre_plaques = argc - 1;
	if (nombre_plaques <= 0)
	{
		std::cout <<
			"format des plaques : x1!x2-y-date.extension ex : 18!19-40-1956.jpg " << std::endl <<
			"il est gere si il n'y a pas de date, et des choses apres la date" << std::endl <<
			"le format opti pour les plaques est 3540*2360" << std::endl <<
			"veuillez drag and drop vos planches IGC sur le .exe pour debuter" << std::endl<<
			"by PEPITO" << std::endl;

		system("PAUSE");
		return -2;
	}

	//Mat mes_plaques[317];
	//Plaque_info info_plaques[317];
	Mat *mes_plaques;
	mes_plaques = new Mat[nombre_plaques];
	Plaque_info *info_plaques;
	info_plaques = new Plaque_info[nombre_plaques];
	float x_max = 0, x_min = 1000, y_max = 0, y_min = 1000;
	//system("PAUSE");
	//chargement des infos


	for (int i = 1; i < nombre_plaques + 1; i++)
	{
		std::cout << "i = " << i << "le nom : " << retire_chemin(argv[i]) << std::endl;
		info_plaques[i - 1] = return_info(retire_chemin(argv[i]));
		if (info_plaques[i - 1].x > x_max)
			x_max = info_plaques[i - 1].x;
		if (info_plaques[i - 1].x < x_min)
			x_min = info_plaques[i - 1].x;
		if (info_plaques[i - 1].y > y_max)
			y_max = info_plaques[i - 1].y;
		if (info_plaques[i - 1].y < y_min)
			y_min = info_plaques[i - 1].y;


	}

	//parametrage de la generation
	Param_Assemblage mes_parametres;
	char parametrage;
	std::cout << "voulez vous le parametrage avec interface y/n" << std::endl;
	std::cin >> parametrage;
	if ((parametrage == 'y') || (parametrage == 'Y') || (parametrage == 'o') || (parametrage == 'O'))
		mes_parametres = return_param_interface(info_plaques, nombre_plaques,argv);
	else
		mes_parametres = return_param_console(info_plaques, nombre_plaques);
	

	Mat output((mes_parametres.y_borne_max - mes_parametres.y_borne_min + 1) * 2360, (mes_parametres.x_borne_max - mes_parametres.x_borne_min + 1) * 3540, CV_8UC3, Scalar(0, 0, 0));

	std::cout << "output cols : " << output.cols << "output rows : " << output.rows << std::endl;

	//system("PAUSE");
	Size size(3540, 2360);
	for (int i = 1; i < nombre_plaques + 1; i++)
	{
		if ((info_plaques[i - 1].x >= mes_parametres.x_borne_min) && (info_plaques[i - 1].x <= mes_parametres.x_borne_max) && (info_plaques[i - 1].y >= mes_parametres.y_borne_min) && (info_plaques[i - 1].y <= mes_parametres.y_borne_max))
		{
			mes_plaques[i - 1] = imread(argv[i], IMREAD_UNCHANGED);
			std::cout << "plaque no : " << i << " charge" << std::endl;
			resize(mes_plaques[i - 1], mes_plaques[i - 1], size);
		}
	}



	//addition du tout 
	std::cout << "on a tout charge " << std::endl;
	//initialize a 120X350 matrix of black pixels:

	for (int i = 0; i < nombre_plaques; i++)
	{
		if ((info_plaques[i].x >= mes_parametres.x_borne_min) && (info_plaques[i].x <= mes_parametres.x_borne_max) && (info_plaques[i].y >= mes_parametres.y_borne_min) && (info_plaques[i].y <= mes_parametres.y_borne_max))
		{
			std::cout << "plaque " << info_plaques[i].x << "-" << info_plaques[i].y << " qui est no : " << i + 1 << " en charge" << std::endl;

			mes_plaques[i].copyTo(output(Rect((info_plaques[i].x - mes_parametres.x_borne_min) * 3540, (info_plaques[i].y - mes_parametres.y_borne_min) * 2360, mes_plaques[i].cols, mes_plaques[i].rows)));

			std::cout << "plaque " << info_plaques[i].x << "-" << info_plaques[i].y << " qui est no : " << i + 1 << " copie" << std::endl;
		}
	}
	imwrite(mes_parametres.nom_output, output);
	std::cout << "fait par pepito " << std::endl;
	system("PAUSE");


	return 0;

}